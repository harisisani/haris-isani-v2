<?php
  $skillArray[]=array(
    "skillName" => 'Core PHP',
    'covered' => '60',
    'icon' => 'stack-exchange',
    'class'=> 'wow fadeInLeft'
  );
  $skillArray[]=array(
    "skillName" => 'Laravel',
    'covered' => '50',
    'icon' => 'weixin',
    'class'=> 'wow fadeInRight'
  );
//   $skillArray[]=array(
//     "skillName" => 'GIT',
//     'covered' => '40',
//     'icon' => 'git',
//   );
//   $skillArray[]=array(
//     "skillName" => 'C#',
//     'covered' => '40',
//     'icon' => 'desktop',
//   );
  $skillArray[]=array(
    "skillName" => 'Asp.Net',
    'covered' => '35',
    'icon' => 'joomla',
    'class'=> 'wow fadeInLeft'
  );

  $skillArray[]=array(
    "skillName" => 'Java',
    'covered' => '30',
    'icon' => 'android',
    'class'=> 'wow fadeInRight'
  );
  $skillArray[]=array(
    "skillName" => 'Javascript',
    'covered' => '60',
    'icon' => 'connectdevelop',
    'class'=> 'wow fadeInLeft'
  );
//   $skillArray[]=array(
//     "skillName" => 'MS SQL',
//     'covered' => '60',
//     'icon' => 'database',
//   );
  $skillArray[]=array(
    "skillName" => 'My SQL',
    'covered' => '40',
    'icon' => 'database',
    'class'=> 'wow fadeInRight'
  );

  $skillArray[]=array(
    "skillName" => 'WordPress',
    'covered' => '60',
    'icon' => 'wordpress',
    'class'=> 'wow fadeInLeft'
  );
  $skillArray[]=array(
    "skillName" => 'HubSpot',
    'covered' => '80',
    'icon' => 'github',
    'class'=> 'wow fadeInRight'
  );

//   $skillArray[]=array(
//     "skillName" => 'Adobe Photoshop',
//     'covered' => '50',
//     'icon' => 'photo',
//   );

  $skillArray[]=array(
    "skillName" => 'API Integration',
    'covered' => '80',
    'icon' => 'wrench',
    'class'=> 'wow fadeInLeft'
  );
//   $skillArray[]=array(
//     "skillName" => 'Salesforce',
//     'covered' => '60',
//     'icon' => 'user-plus',
//   );
  $skillArray[]=array(
    "skillName" => 'Bootstrap',
    'covered' => '80',
    'icon' => 'ticket',
    'class'=> 'wow fadeInRight'
  );
  $skillArray[]=array(
    "skillName" => 'HTML',
    'covered' => '90',
    'icon' => 'html5',
    'class'=> 'wow fadeInLeft'
  );
  $skillArray[]=array(
    "skillName" => 'CSS',
    'covered' => '85',
    'icon' => 'css3',
    'class'=> 'wow fadeInRight'
  );
//   $skillArray[]=array(
//     "skillName" => 'MS Office',
//     'covered' => '95',
//     'icon' => 'user',
//   );
?>
<!DOCTYPE html>
 <html lang="zxx">

    <head>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="keywords" content="Haris Isani" />
        <meta name="description" content="Hello, I am Haris Isani, and I am professionally a Software Engineer. I have a specialized history working as a Software Developer, Analyst & QA. Some of my previous projects have given me experience in web applications such as Core PHP, Laravel, ASP.Net, HubSpot CMS, WordPress, HTML, CSS, JavaScript etc." />
        <meta name="author" content="Haris Isani" />

        <!-- Title  -->
        <title>Haris Isani</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.png" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:300,400,500,600,700" rel="stylesheet">

        <!-- Plugins -->
        <link rel="stylesheet" href="css/plugins.css" />
        <link rel="stylesheet" href="css/all.min.css" />

        <!-- Core Style Css -->
        <link rel="stylesheet" href="css/style.css" />

    </head>

    <body>

        <!-- =====================================
        ==== Start Loading -->

        <!-- <div class="loading">
            <div class="load-circle">
            </div>
        </div> -->

        <!-- End Loading ====
        ======================================= -->


        <!-- =====================================
        ==== Start Navbar -->
        <nav style="max-width:100vw; background: linear-gradient(-45deg, teal, #8089ff)" class="navbar navbar-expand-lg">
            <div class="container">

            <!-- Logo -->
            <!-- <a class="logo" href="index.php">
                <img src="img/logo.png" alt="logo">
            </a> -->

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"><i class="fas fa-bars"></i></span>
              </button>

              <!-- navbar links -->
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link active" href="#" data-scroll-nav="0">HOME</a>
                  </li>
                  <li class="nav-item light">
                    <a class="nav-link light" href="#" data-scroll-nav="1">ABOUT</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="2">SERVICES</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="3">PORTFOLIO</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="4">TESTIMONIALS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="5">SKILLS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="6">RESUME</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="7">CONTACT</a>
                  </li>
                </ul>
              </div>

                <ul class="header-icon">
                  <li><a target="_blank" href="https://harisisani.clientpoint.net/proposal/unlock-view/proposalId/592671/pin/5123"><i class="fa fa-map-marker"></i></a></li>
                  <li><a target="_blank" href="https://www.facebook.com/HARIS.ISANI/"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a target="_blank" href="https://www.linkedin.com/in/haris-isani/"><i class="fab fa-linkedin"></i></a></li>
                  <li><a target="_blank" href="https://www.youtube.com/channel/UCcHo-NQw4HzNFzdCnZGMJTg"><i class="fab fa-youtube"></i></a></li>
                  <li><a target="_blank" href="https://gitlab.com/harisisani"><i class="fab fa-git"></i></a></li>
                </ul>
              </div>
            </div>
        </nav>

        <!-- End Navbar ====
        ======================================= -->


        <!-- =====================================
        ==== Start Header -->

        <header style="max-width:100vw;" class="header slider-fade slider-style-1" data-scroll-index="0">
                <div class="item bg-img" data-overlay-dark="5" data-background="img/slider/haris-isani-slider.jpg">
                    <div class="v-middle caption">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="o-hidden mw-800">
                                        <h1 class="mt-15 mb-15 cd-headline clip is-full-width">
                                            <div class="wow fadeInUp">
                                                <span class="blc">I am </span>
                                                <span class="cd-words-wrapper waiting">
                                                <b class="is-visible">Haris Isani</b>
                                                <b>Software Engineer</b>
                                                <b>Freelancer</b>
                                                </span>
                                            </div>
                                        </h1>
                                        <p class="slider-text text-center">I Love coding & believe in solving complex problems with simple solutions. It's my mission & purpose to turn every legacy into a modern solution</p>
                                        <a target="_blank" href="https://www.upwork.com/fl/harisisani" class="butn butn-bord-trans mt-50">
                                            <span>Hire Me</span>
                                        </a>
                                        <a target="_blank" href="./resume/Haris Isani - Resume.pdf" class="butn butn-tra butn-bord-trans mt-50">
                                            <span>Download CV</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </header>

        <!-- End Header ====
        ======================================= -->


             <!-- =====================================
        ==== Start About -->
        <section style="max-width:100vw;"  id="about" class="about" data-scroll-index="1">
            <div class="container">
                <div class="row">
                    <div class="wow fadeInLeft col-lg-5">
                        <div class="img">
                            <img src="img/about/haris-isani-min.png" alt="">
                        </div>
                    </div>

                    <div class="wow fadeInRight col-lg-7">
                        <div class="info section-padding">
                            <div class="wow fadeInUp">
                                <h5>MY INTRO</h5>
                                <h2>About Me</h2>
                            </div>
                            <p>Hello, I am Haris Isani, and I am professionally a Software Engineer.</p>
                            <p>I have a specialized history working as a Software Developer, Analyst & QA. Some of my previous projects have given me experience in web applications such as Core PHP, Laravel, ASP.Net, HubSpot CMS, WordPress, HTML, CSS, JavaScript etc.</p>
                            <p>I have developed APIs & Integrated a few CRMs to my applications via APIs.</p>
                            <p>I explored some of the DevOps platforms like Azure DevOps, Basecamp, Trello, etc.</p>
                            <a target="_blank" href="https://www.facebook.com/HARIS.ISANI/" class="butn">
                                <span><i class="fab fa-facebook-f"></i></span>
                            </a>
                            <a target="_blank" href="https://www.linkedin.com/in/haris-isani/" class="butn">
                                <span><i class="fab fa-linkedin"></i></span>
                            </a>
                            <a target="_blank" href="https://www.youtube.com/channel/UCcHo-NQw4HzNFzdCnZGMJTg" class="butn">
                                <span><i class="fab fa-youtube"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
            <div class="row">
                <div class="wow fadeInRight col-lg-5">
                    <div class="info section-padding">
                        <div class="wow fadeInUp">
                            <h5>MY INTRO</h5>
                            <h2>Narrative</h2>
                        </div>
                        <p>My field of study is constituted primarily of a bachelor's degree in Computer Science from a reputable higher education institute.</p>
                        <p>I have completed numerous internships and certifications that not only enabled me to gain important field knowledge but also a way to represent myself on a publicly scaled platform.</p>
                        <p>These experiences and my adaptable background have given me the ability to design front-end work, create programming logic, derive service system flows, design business brandings, designing business proposals, and much more</p>
                    </div>
                </div>
                <div class="wow fadeInLeft col-lg-7">
                    <div class="img">
                         <iframe class="responsive-video" style="width: 100%;height: auto;" src="https://www.youtube.com/embed/2vuow0S3b2U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <!-- End About ====
        ======================================= -->



<!-- =====================================
        ==== Start services -->
    <section style="max-width:100vw;"  id="services" class="services text-center bg-gray section-padding" data-scroll-index="2">
            <div class="container">
                <div class="section-head">
                    <div class="wow fadeInUp">
                        <h4>Services</h4>
                        <p>I do awesome services for clients. A few of them are</p>
                    </div>
                </div>
                <div class="row">
                    <div class="wow fadeInLeft col-md-4 col-sm-12 col-xs-12 ">
                        <div class="box-icon-left items">
                            <span class="icon"><i class="fa fa fa-paint-brush"></i></span>
                            <div class="cont">
                                <h5 class="mb-10">Web Development</h5>
                            </div>
                        </div>
                    </div>
                    <div class="wow fadeInUp col-md-4 col-sm-12 col-xs-12">
                        <div class="box-icon-left items">
                            <span class="icon"><i class="fas fa-magic"></i></span>
                            <div class="cont">
                                <h5 class="mb-10">Web Services</h5>
                            </div>
                        </div>
                    </div>
                    <div class="wow fadeInRight col-md-4 col-sm-12 col-xs-12">
                        <div class="box-icon-left items">
                            <span class="icon"><i class="fab fa-odnoklassniki"></i></span>
                            <div class="cont">
                                <h5 class="mb-10">API Integration</h5>
                            </div>
                        </div>
                    </div>

                    <div class="wow fadeInLeft col-md-4 col-sm-12 col-xs-12 ">
                        <div class="box-icon-left items">
                            <span class="icon"><i class="fas fa-university"></i></span>
                            <div class="cont">
                                <h5 class="mb-10">POS Development</h5>
                                <!-- <p class="pb-10">A decade ago, we founded Melinda with the goal of creating meaningful digital experiences.</p> -->
                            </div>
                        </div>
                    </div>
                    <div class="wow fadeInDown col-md-4 col-sm-12 col-xs-12">
                        <div class="box-icon-left items">
                            <span class="icon"><i class="fas fa-chart-line"></i></span>
                            <div class="cont">
                                <h5 class="mb-10">CMS Development</h5>
                            </div>
                        </div>
                    </div>
                    <div class="wow fadeInRight col-md-4 col-sm-12 col-xs-12">
                        <div class="box-icon-left items">
                            <span class="icon"><i class="fas fa-braille"></i></span>
                            <div class="cont">
                                <h5 class="mb-10">E-Commerce</h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- End services ====
        ======================================= -->



        <!-- =====================================
        ==== Start Works -->

    <section id="portfolio" class="works section-padding" data-scroll-index="3">
        <div class="container-fluid">
            <div class="row">

                <div class="section-head text-center col-sm-12">
                    <div class="wow fadeInUp">
                        <h4>Featured Work</h4>
                        <p>I did many awesome projects for my clients</p>
                    </div>
                </div>

                <!-- filter links -->
                <div class="wow fadeInDown filtering text-center mb-20 col-sm-12">
                    <div class="filter">
                        <span data-filter='*' class="active">ALL</span>
                        <span data-filter='.brand'>Web Development</span>
                        <span data-filter='.design'>Custom Development</span>
                        <span data-filter='.development'>E-Commerce</span>
                        <span data-filter='.marketing'>API Integration</span>
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- gallery -->
                <div class="gallery full-width">

                 <!-- gallery item -->
                 <div class="col-lg-3 col-md-6 items design">
                        <div class="item-img">
                            <img src="img/portfolio/mobetter.png" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="https://www.mobetterhealthcare.com/">Mo Better HealthCare</a></h3>
                                        <p>Custom Development</p>
                                    </div>
                                    <a href="img/portfolio/mobetter.png" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                     <!-- gallery item -->
                     <div class="col-lg-3 col-md-6 items brand">
                        <div class="item-img">
                            <img src="img/portfolio/hiveint.png" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="https://hiveint.net/">Hive International</a></h3>
                                        <p>Web Development</p>
                                    </div>
                                    <a href="img/portfolio/hiveint.png" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-6 items brand">
                        <div class="item-img">
                            <img src="img/portfolio/1.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                    <h3 style="color: #fff;">Customized Web-Application</h3>
                                        <p>Web Development</p>
                                    </div>
                                    <a href="img/portfolio/1.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-6 items brand">
                        <div class="item-img">
                            <img src="img/portfolio/2.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3 style="color: #fff;">Digital Virtual Examiner</h3>
                                        <p>Web Development</p>
                                    </div>
                                    <a href="img/portfolio/2.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-6 items development">
                        <div class="item-img">
                            <img src="img/portfolio/3.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="https://rexbazar.pk/">E-Commerce Website</a></h3>
                                        <p>E-Commerce</p>
                                    </div>
                                    <a href="img/portfolio/3.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-6 items design">
                        <div class="item-img">
                            <img src="img/portfolio/4.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                    <h3><a target="_blank" href="https://youtu.be/vhRmosPEiXg">Hangman Game</a></h3>
                                        <p>Custom Development</p>
                                    </div>
                                    <a href="img/portfolio/4.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-6 items brand">
                        <div class="item-img">
                            <img src="img/portfolio/5.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="https://info.inspace.chat/product-features">inSpace CMS based Website</a></h3>
                                        <p>Web Development</p>
                                    </div>
                                    <a href="img/portfolio/5.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-6 items marketing">
                        <div class="item-img">
                            <img src="img/portfolio/6.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="https://www.virustotal.com/">VirusTotal API </a></h3>
                                        <p>API Integration</p>
                                    </div>
                                    <a href="img/portfolio/6.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-4 items design">
                        <div class="item-img">
                            <img src="img/portfolio/7.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="http://southlaneanimalhospital.com/south-lane">South-Lane Hospital POS Portal</a></h3>
                                        <p>POS Development</p>
                                    </div>
                                    <a href="img/portfolio/7.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>



                    <!-- gallery item -->
                    <div class="col-lg-3 col-md-4 items design">
                        <div class="item-img">
                            <img src="img/portfolio/8.jpg" alt="image">
                            <div class="item-img-overlay">
                                <div class="overlay-info full-width">
                                    <div class="cont v-middle">
                                        <h3><a target="_blank" href="https://www.genderhealthtraining.com/">Gender Health's CMS based Website</a></h3>
                                        <p>CMS Development</p>
                                    </div>
                                    <a href="img/portfolio/8.jpg" class="popimg">
                                        <span class="icon"><i class="fas fa-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </section>

    <!-- End Works ====
    ======================================= -->


    <!-- =====================================
    ==== Start Testimonials-Grid -->

    <section class="testimonials-grid section-padding bg-img bg-fixed bg-gray" data-scroll-index="4">
        <div class="container">
            <div class="row">

                <div class="section-head text-center col-sm-12">
                    <div class="wow fadeInUp">
                        <h4>TESTIMONIES</h4>
                        <p>What client says about?</p>
                    </div>
                </div>

                <div class="wow fadeInDown owl-carousel owl-theme col-lg-12">
                        <div class="item text-center">
                        <div class="info">
                            <h6>Shawn V. Giammattei <span>Owner & Founder at Gender Health Training Institute</span></h6>
                            <!-- <div class="img">
                                <img src="img/clients/1.jpg" alt="">
                            </div> -->
                        </div>
                        <p>Haris has been helping me build a new Wordpress site and clean up an older site, as well as creating graphics for social media campaigns. His work has been stellar and communication excellent. I would highly recommend him!</p>
                        <div class="stars">
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="info">
                            <h6>Carolina Sirena <span>Professional Author</span></h6>
                            <!-- <div class="img">
                                <img src="img/clients/1.jpg" alt="">
                            </div> -->
                        </div>
                        <p>Haris has communicated professionally all the time, he responded all mails in a short amount fo time and was very friendly. He incorporated my feedback without hesitation (more times than agreed in the contract).</p>
                        <div class="stars">
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <!-- <span><i class="fas fa-star-half-alt"></i></span> -->
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="info">
                            <h6>Kathleen Saenz <span>Client-Success & Marketing Specialist</span></h6>
                        </div>
                        <p>Haris is by far the best developer I've ever had the pleasure of working with.  My favorite project that Haris and I worked on together was for a highly complex pricing tool, involving hundreds of products, equations, and variables.<br/></p>
                        <div class="stars">
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="info">
                            <h6>Shahid Sheikh <span>CMS & SEO Specialist</span></h6>
                        </div>
                        <p>I have been in the IT field for the last 10 years. I know Haris for almost 3 years and we have produced some real quality software and websites together. The best thing I found out in Haris is the way he communicates.</p>
                        <div class="stars">
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="info">
                            <h6>Syed Turab Naqvi <span>Document Processor & Designer</span></h6>
                        </div>
                        <p>Haris is very talented, very skillful, and a problem-solving guy who is always punctual, on time, communicates properly, and always tries to give a solution for a typical or complicated task.</br></br></br></br></p>
                        <div class="stars">
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="info">
                            <h6>Hamza Khan <span>IT Specialist</span></h6>
                        </div>
                        <p>We developed a good partnership. I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. You guys are reliable, flexible and very responsive.</br></br></p>
                        <div class="stars">
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star"></i></span>
                            <span><i class="fas fa-star-half-alt"></i></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

        <style>
            .skills-grid{
                background:#F5F2FC;
                padding:20px;
                color:#191919;
                border: 1px solid rgba(0, 0, 0, 0.10);

            }
            .skills-grid:hover{
                box-shadow: 0 0.3rem 0.3rem rgb(0 0 0 / 15%) !important;

            }
        </style>
        <!-- End Testimonials-Grid ====
        ======================================= -->
        <section class="testimonials-grid section-padding bg-img bg-fixed bg-gray" style="background:#FFFFFF;" data-scroll-index="5">
		<div class="container">
			<div class="row">
                <div class="section-head text-center col-sm-12">
                    <div class="wow fadeInUp">
                        <h4>SKILLS</h4>
                        <p>My Attained Skills</p>
                    </div>
                </div>
			</div>
            <div class="row">
			<div class="row progress-circle mb-5">
				<?php foreach($skillArray as $value){?>
				<div class="<?=$value['class']?> col-lg-4 col-md-4 col-xs-6 col-sm-6 mb-10">
					<div class="skills-grid rounded-lg shadow">
					<h2 class="h5 font-weight-bold text-center"><?=$value['skillName']?>
                        <!-- &nbsp;&nbsp;<i class="fa fa fa-</?=$value["icon"]?>"></i> -->
                    </h2>
                    <div class="skills-progress">
                        <span data-value="<?=$value['covered']?>%" style="width: <?=$value['covered']?>%;"></span>
                    </div>

						<!-- Progress bar 1 -->
						<!-- <div class="progress mx-auto" data-value='</?=$value['covered']?>'>
							<span class="progress-left">
								<span class="progress-bar border-primary"></span>
							</span>
							<span class="progress-right">
								<span class="progress-bar border-primary"></span>
							</span>
							<div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
								<div class="h2 font-weight-bold"></?=$value['covered']?><sup class="small">%</sup></div>
							</div>
						</div> -->
						<!-- END -->
					</div>
				</div>
				<?php }?>
			</div>
		</div>
	</section>


        <!-- =====================================
        ==== Start Blog -->
        <section id="blog" class="blog section-padding" data-scroll-index="6" style="background: #F5F2FC !important;max-width:100vw;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="section-head text-center col-sm-12">
                        <div class="wow fadeInUp">
                            <h4>My Resume</h4>
                            <p>Have a fair interest in Programming languages and Computers. I want to work in a collaborative environment where I can make use of my speedy work, professional skills and explore new technologies. I am willing to learn more to grow more in my reputative field.</p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex">
                    <div class="col-lg-4 wow fadeInRight">
                            <div class="section-resume">
                                <div class="section-head text-center col-sm-12">
                                    <h4 style="font-size:30px">EDUCATION</h4>
                                </div>
                                <div class="cont">
                                    <h6 class="mb-20 mt-20">Bachelor of Science Computer Science&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</h6>
                                </div>
                                <p class="mb-10"><a target="_blank" style="color:teal" href="https://iqra.edu.pk/">- Iqra University, Karachi</a></p>
                                <h3 class="heading"></h3>
                                <div class="cont">
                                    <h6 class="mb-20">Intermediate: Computer Science</h6>
                                </div>
                                <p class="mb-10">- Govt. College for Men, Karachi</p>
                                <div class="cont">
                                    <h6 class="mb-20">Matric: Computer Science</h6>
                                </div>
                                <p class="mb-20">- S.M Public Academy, Karachi</p>
                            </div>
                    </div>
                    <div class="col-lg-4 wow fadeInDown">
                            <div class="section-resume">
                            <div class="section-head text-center col-sm-12">
                                    <h4 style="font-size:30px">EXPERIENCE</h4>
                                </div>
                                <div class="cont">
                                    <h6 class="mb-20 mt-20">Software Developer & Quality Assurance (2019 - Current)</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://codingkey.com/">- CodingKey, Karachi</a></p>
                                <div class="cont">
                                    <h6 class="mb-20">Freelancer (2021 - Current)</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://www.upwork.com/freelancers/harisisani">- Upwork</a></p>
                                <div class="cont">
                                    <h6 class="mb-20">Technical VA (2021)</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://www.genderhealthtraining.com/">- Gender Health Training Institute, USA</a></p>
                                <div class="cont">
                                    <h6 class="mb-20">Intern Document Processing & Software Quality Assurance (2019)</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://tc-bpo.com/">- Tribe Consulting, Karachi</a></p>
                            </div>
                    </div>
                    <div class="col-lg-4 wow fadeInLeft">
                            <div class="section-resume">
                                <div class="section-head text-center col-sm-12">
                                    <h4 style="font-size:30px">CERTIFICATIONS</h4>
                                </div>
                                <div class="cont">
                                    <h6 class="mb-20 mt-20">Growth Driven Design, CMS for developers, Email Marketing</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://academy.hubspot.com/">- HubSpot Academy</a></p>
                                <div class="cont">
                                    <h6 class="mb-20">HTML, CSS, WordPress, jQuery, MySql, JSON, GIT, Microsoft Azure</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://www.linkedin.com/in/haris-isani/">- LinkedIn Accessment</a></p>
                                <div class="cont">
                                    <h6 class="mb-20">Frontend Fundamentals</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://www.pirple.com/">- Pirple Thinkific</a></p>
                                <div class="cont">
                                    <h6 class="mb-20">Database Concepts</h6>
                                </div>
                                <p class="mb-20"><a target="_blank" style="color:teal" href="https://www.facebook.com/IUEcompetencia/">- ECOMPENTENCIA</a></p>
                            </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Blog ====
        ======================================= -->


        <!-- =====================================
        ==== Start Contact -->

        <section style="max-width:100vw;" id="contact" class="page-contact mt-100" data-scroll-index="7">
            <div class="container">
                <div class="wow fadeInUp section-head text-center col-sm-12">
                    <h4>Have a Project?</h4>
                    <p>Feel free to Connect</p>
                </div>

                <div class="row">
                    <div class="wow fadeInLeft col-lg-8">
                        <form style="padding:20px;background: #F8F9FA;" class="form">

                            <div class="messages"></div>

                            <div class="controls">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <i class="far fa-user"></i>
                                            <input id="form_name" type="text" name="sender_name" name="sender_name" placeholder="Name..." required="required" data-error="Firstname is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <i class="far fa-envelope"></i>
                                            <input id="form_subject" type="email" name="sender_email" placeholder="Email Address...">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <i class="far fa-comment-dots"></i>
                                            <input id="form_subject" type="text" name="subject" placeholder="Subject...">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <i class="far fa-comment"></i>
                                            <textarea  id="message" name="message" placeholder="Your Message" rows="4" required="required" data-error="Your message is required."></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <but onclick="sendResponse();" type="submit" class="butn butn-bg"><span>Send Message</span></but>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div style="padding:20px;" class="wow fadeInRight col-lg-4">
                        <div class="item row">
                            <span class="icon col-2 col-sm-2"><i class="fas fa-anchor"></i></span>
                            <div class="content col-10 col-sm-10">
                                <h3>Upwork</h3>
                                <br>
                                <p><a target="_blank" href="https://www.upwork.com/fl/harisisani">Profile</a></p>
                            </div>
                        </div>

                        <div class="item row">
                            <span class="icon col-2 col-sm-2"><i class="fas fa-map-marker"></i></span>
                            <div class="content col-10 col-sm-10">
                                <h3>Office</h3>
                                <br>
                                <p><a target="_blank" href="https://harisisani.clientpoint.net/proposal/unlock-view/proposalId/592671/pin/5123">Virtual Office</a></p>
                            </div>
                        </div>
                        <div class="item row">
                            <span class="icon col-2 col-sm-2"><i class="fas fa-mobile-alt"></i></span>
                            <div class="content col-10 col-sm-10">
                                <h3>Let's Talk</h3>
                                <p>Mobile : +92344-2329735</p>
                            </div>
                        </div>

                        <div class="item row">
                            <span class="icon col-2 col-sm-2"><i class="far fa-envelope"></i></span>
                            <div class="content col-10 col-sm-10">
                                <h3>E-mail</h3>
                                <br>
                                <p>harisisani@gmail.com</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <!-- End Contact ====
        ======================================= -->
        <!-- =====================================
        ==== Start Footer -->

        <footer  style="max-width:100vw;" class="footer">

            <div class="sub-footer">
                <div style="text-align:center;" class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Copyright &copy; 2022 Haris Isani. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- End Footer ====
        ======================================= -->


        <!-- jQuery -->
        <script src="js/jquery-3.0.0.min.js"></script>
        <script src="js/jquery-migrate-3.0.0.min.js"></script>

        <!-- popper.min -->
        <script src="js/popper.min.js"></script>

        <!-- bootstrap -->
        <script src="js/bootstrap.min.js"></script>

        <!-- scrollIt -->
        <script src="js/scrollIt.min.js"></script>

        <!-- jquery.waypoints.min -->
        <script src="js/jquery.waypoints.min.js"></script>

        <!-- jquery.counterup.min -->
        <script src="js/jquery.counterup.min.js"></script>

        <!-- circle-progress.min -->
        <script src="js/circle-progress.min.js"></script>

        <!-- owl carousel -->
        <script src="js/owl.carousel.min.js"></script>

        <!-- jquery.magnific-popup js -->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!-- stellar js -->
        <script src="js/jquery.stellar.min.js"></script>

        <!-- jquery.youtubebackground.js -->
        <script src="js/jquery.youtubebackground.js"></script>

        <!-- animated.headline -->
        <script src="js/animated.headline.js"></script>

        <!-- isotope.pkgd.min js -->
        <script src="js/isotope.pkgd.min.js"></script>

        <!-- YouTubePopUp.jquery -->
        <script src="js/YouTubePopUp.jquery.js"></script>

        <!-- validator js -->
        <script src="js/validator.js"></script>

        <!-- custom scripts -->
        <script src="js/scripts.js"></script>
        <script src="js/wow.js"></script>
    </body>
</html>
<style>

iframe.responsive-video{
        min-height:80vh;
	}

    .section-resume{
        max-width: 100%;
        border: 1px solid rgba(0, 0, 0, 0.10);
        padding: 25px;
        background: white;
        border-radius: 0.3rem !important;
        box-shadow: 0 0.5rem 1rem rgb(0 0 0 / 15%) !important;
        position: relative;
        min-height: 550px;
        margin-bottom: 50px;
    }
  @media screen and (min-width: 989px) {
	iframe.responsive-video{
        min-height:600px;
	}
    .section-resume{
        max-height: 350px;
    }
  }
</style>
<script>
    function sendResponse(){
    var settings = {
        "url": "./email/sendmail.php",
        "method": "POST",
        "timeout": 0,
        "headers": {
        "Content-Type": "application/json"
    },
        "data": JSON.stringify({
        "sender_email": ""+$('input[name="sender_email"]').val(),
        "sender_name": ""+$('input[name="sender_name"]').val(),
        "subject": ""+$('input[name="subject"]').val(),
        "message": ""+$('#message').val(),
        }),
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        alert("Response Received");
        $('input[name="sender_email"]').val("");
        $('input[name="sender_name"]').val("");
        $('input[name="subject"]').val("");
        $('#message').val("");
        });
    }

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if(scroll<=200){
            wow.init();
        }
    });


</script>